<?php

namespace Dense\Intruder;

use Illuminate\Support\ServiceProvider;

use Dense\Intruder\Commands\Generate;

class IntruderServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    protected $namespace = 'intruder';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // commands
        $this->commands([
            Generate::class,
        ]);

        // langs
        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/lang');

        // database migrations
        $this->loadMigrationsFrom(__DIR__ . '/database');

        // routes
        $this->loadRoutesFrom(__DIR__ . '/routes/intruder.php');

        // config
        $this->publishes([
            __DIR__ . '/config/intruder.php' => config_path('intruder.php'),
        ]);

        $this->mergeConfigFrom(__DIR__ . '/config/intruder.php', 'intruder');

        // langs
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang', $this->namespace);

        $this->publishes([
            __DIR__ . '/resources/lang' => resource_path('lang/vendor/intruder'),
        ]);

        // views
        $this->loadViewsFrom(__DIR__ . '/resources/views', $this->namespace);

        $this->publishes([
            __DIR__ . '/resources/views' => resource_path('views/vendor/intruder'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
