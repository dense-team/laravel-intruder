<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Dense\Enum\Status;
use Dense\Enum\Role;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integerIncrements('user_id');
            $table->string('identifier', 20)->nullable();
            $table->string('forename', 255);
            $table->string('surname', 255);
            $table->string('email', 255)->unique();
            $table->string('phone', 50)->nullable();
            $table->string('password', 255);
            $table->string('status', 50)->default(Status::STATUS_ACTIVE);
            $table->string('role', 50)->default(Role::ROLE_REGULAR);
            $table->string('remember_token', 100)->nullable();
            $table->string('api_token', 100)->nullable();
            $table->timestamp('email_verified_at', null)->nullable();
            $table->timestamp('updated_at', null)->nullable();
            $table->timestamp('created_at', null)->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
