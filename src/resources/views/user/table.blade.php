@extends('layouts.app')

@section('content')
    <h1>{{ __('intruder::account.user_list') }}</h1>

    @include('assistant::report.all')

    @can('edit', \App\Model\User\User::class)
    <p>
        <a href="{{ route('user.edit') }}" class="btn btn-primary">
            <span class="material-icons">add</span>
            <span class="align-middle">{{ __('assistant::general.add') }}</span>
        </a>
    </p>
    @endcan

    @if(!$users->isEmpty())
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th style="width: 50px;">#</th>
                    <th>{{ __('intruder::account.name') }}</th>
                    <th>{{ __('intruder::account.email') }}</th>
                    <th>{{ __('intruder::account.role') }}</th>
                    <th>{{ __('intruder::account.status') }}</th>
                    <th style="width: 130px;"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users->values() as $idx => $user)
                    <tr>
                        <td>{{ $idx + 1 }}</td>
                        <td>{{ $user->getName() }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ \App\Model\User\UserRole::getEnum($user->role) }}</td>
                        <td>{{ \Dense\Enum\Status::getEnum($user->status) }}</td>
                        <td class="no-wrap">
                            @can('edit', \App\Model\User\User::class)
                                <a class="btn btn-success" href="{{ route('user.edit', ['user_id' => $user->user_id]) }}" title="{{ __('assistant::general.edit') }}">
                                    <span class="material-icons">edit</span>
                                </a>
                            @endcan
                            @can('delete', \App\Model\User\User::class)
                                <a class="btn btn-danger" href="{{ route('user.delete', ['user_id' => $user->user_id]) }}" title="{{ __('assistant::general.delete') }}" @confirmDelete>
                                    <span class="material-icons">delete</span>
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection
