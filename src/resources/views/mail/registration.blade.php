@component('mail::message')

User with name <strong>{{ $user->getName() }}</strong> has just registered on site {{ config('app.name') }}.
<br />
To activate his account click the following link: <a href="{{ route('user.activate', ['user_id' => $user->user_id]) }}">activate account</a>.

@endcomponent
