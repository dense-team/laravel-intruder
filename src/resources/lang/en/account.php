<?php

return [
    'user' => 'User',
    'users' => 'Users',
    'user_list' => 'List of Users',
    'account' => 'Account',
    'name' => 'Name',
    'forename' => 'Forename',
    'surname' => 'Surname',
    'email' => 'Email',
    'phone' => 'Phone',
    'password' => 'Password',
    'password_confirmation' => 'Password confirmation',
    'login' => 'Login',
    'logout' => 'Logout',
    'registration' => 'Registration',
    'role' => 'Role',
    'status' => 'Status',
    'api_token' => 'API token',
    'generate_token' => 'Generate API token',
];
