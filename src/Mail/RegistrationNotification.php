<?php

namespace Dense\Intruder\Mail;

use Illuminate\Mail\Mailable;

class RegistrationNotification extends Mailable
{
    /**
     * @var \Dense\Intruder\User\User;
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @var \Dense\Intruder\User\User $user;
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = config('app.name') . ' - ' .  __('Registration notification');

        return $this
            ->subject($subject)
            ->markdown('intruder::mail.registration');
    }
}
