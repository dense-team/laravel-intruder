<?php

Route::namespace('App\Http\Controllers\Intruder')->middleware(['web'])->group(function () {

    // account
    Route::match(['get', 'post'], 'account', ['as' => 'account', 'uses' => 'AccountController@account']);

    Route::get('token', ['as' => 'token', 'uses' => 'AccountController@token']);

    // users
    Route::get('users', ['as' => 'user.table', 'uses' => 'UserController@table']);

    Route::match(['get', 'post'], 'user-edit/{user_id?}', ['as' => 'user.edit', 'uses' => 'UserController@edit']);

    Route::get('user-delete/{user_id}', ['as' => 'user.delete', 'uses' => 'UserController@delete']);

    Route::get('user-activate/{user_id}', ['as' => 'user.activate', 'uses' => 'UserController@activate']);
});
