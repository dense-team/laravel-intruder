<?php

namespace Dense\Intruder\Registration\Subscribers;

use Illuminate\Support\Facades\Mail;

use Dense\Intruder\Mail\RegistrationNotification;

class RegistrationSubscriber
{
    /**
     * Handle user register events.
     */
    public function onUserRegister($event) {
        $address = config('intruder.email');

        Mail::to($address)
            ->send(new RegistrationNotification($event->user));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Registered',
            'Dense\Intruder\Registration\Subscribers\RegistrationSubscriber@onUserRegister'
        );
    }
}
