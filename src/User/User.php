<?php

namespace Dense\Intruder\User;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;

use Dense\Intruder\User\Contracts\HasRole as HasRoleContract;
use Dense\Intruder\User\Contracts\HasStatus as HasStatusContract;
use Dense\Intruder\User\Contracts\HasApiToken as HasApiTokenContract;
use Dense\Intruder\User\Contracts\CanVerifyEmail as CanVerifyEmailContract;

class User implements HasRoleContract, HasStatusContract, HasApiTokenContract, CanVerifyEmailContract, AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, HasRole, HasStatus, HasApiToken, CanVerifyEmail, CanResetPassword, Notifiable;

    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string
     */
    public $forename;

    /**
     * @var string
     */
    public $surname;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $phone;

    /**
     * @var string
     */
    public $updated_at;

    /**
     * @var string
     */
    public $created_at;

    /**
     * @return string
     */
    public function getName()
    {
        return trim(ucfirst(mb_strtolower($this->forename)) . ' ' . ucfirst(mb_strtolower($this->surname)));
    }

    /**
     * @param array $data
     * @return $this
     */
    public function hydrate(array $data)
    {
        if (array_key_exists('user_id', $data)) {
            $this->user_id = (int)$data['user_id'];
        }
        if (array_key_exists('identifier', $data)) {
            $this->identifier = $data['identifier'] ?? null;
        }
        if (array_key_exists('forename', $data)) {
            $this->forename = $data['forename'];
        }
        if (array_key_exists('surname', $data)) {
            $this->surname = $data['surname'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('phone', $data)) {
            $this->phone = $data['phone'] ?? null;
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        if (array_key_exists('role', $data)) {
            $this->role = $data['role'];
        }
        if (array_key_exists('status', $data)) {
            $this->status = $data['status'];
        }
        if (array_key_exists('remember_token', $data)) {
            $this->remember_token = $data['remember_token'];
        }
        if (array_key_exists('api_token', $data)) {
            $this->api_token = $data['api_token'] ?? null;
        }
        if (array_key_exists('email_verified_at', $data)) {
            $this->email_verified_at = $data['email_verified_at'] ? date('Y-m-d H:i:s', strtotime($data['email_verified_at'])) : null;
        }
        if (array_key_exists('updated_at', $data)) {
            $this->updated_at = $data['updated_at'] ? date('Y-m-d H:i:s', strtotime($data['updated_at'])) : null;
        }
        if (array_key_exists('created_at', $data)) {
            $this->created_at = date('Y-m-d H:i:s', strtotime($data['created_at']));
        }

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [
            'user_id' => $this->user_id,
            'identifier' => $this->identifier,
            'forename' => $this->forename,
            'surname' => $this->surname,
            'email' => $this->email,
            'phone' => $this->phone,
            'password' => $this->password,
            'role' => $this->role,
            'status' => $this->status,
            'api_token' => $this->api_token,
            'email_verified_at' => $this->email_verified_at,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ];

        return $data;
    }
}
