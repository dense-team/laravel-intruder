<?php

/**
 * User: Maros Jasan
 * Date: 21.11.2019
 * Time: 11:06
 */

namespace Dense\Intruder\User;

trait Authenticatable
{
    use PasswordManage, RememberTokenManage;

    /**
     * @var int
     */
    public $user_id;

    /**
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'user_id';
    }

    /**
     * @return int
     */
    public function getAuthIdentifier()
    {
        return $this->user_id;
    }
}
