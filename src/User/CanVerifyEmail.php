<?php
/**
 * User: Maros Jasan
 * Date: 21.11.2019
 * Time: 13:44
 */

namespace Dense\Intruder\User;

trait CanVerifyEmail
{
    /**
     * @var string
     */
    public $email_verified_at;

    /**
     * @return $this
     */
    public function verifyUserEmail()
    {
        $this->email_verified_at = time();

        return $this;
    }
}
