<?php

namespace Dense\Intruder\User\Subscribers;

use Illuminate\Support\Facades\Mail;

use Dense\Intruder\Mail\ActivationNotification;

class UserSubscriber
{
    /**
     * Handle user register events.
     */
    public function onUserActivate($event) {
        $address = $event->user->email;

        Mail::to($address)
            ->send(new ActivationNotification($event->user));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Dense\Intruder\User\Events\ActivateUser',
            'Dense\Intruder\User\Subscribers\UserSubscriber@onUserActivate'
        );
    }
}
