<?php
/**
 * User: Maros Jasan
 * Date: 21.11.2019
 * Time: 14:00
 */

namespace Dense\Intruder\User;

use Dense\Enum\Status as UserStatus;

trait HasStatus
{
    /**
     * @var string
     */
    public $status = UserStatus::STATUS_INACTIVE;

    /**
     * @return bool
     */
    public function hasActiveStatus()
    {
        return $this->status === UserStatus::STATUS_ACTIVE;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->hasActiveStatus();
    }

    /**
     * @return bool
     */
    public function isNotActive()
    {
        return !$this->hasActiveStatus();
    }

    /**
     * @return bool
     */
    public function hasInactiveStatus()
    {
        return $this->status === UserStatus::STATUS_INACTIVE;
    }

    /**
     * @return bool
     */
    public function isInactive()
    {
        return $this->hasInactiveStatus();
    }

    /**
     * @return bool
     */
    public function isNotInactive()
    {
        return !$this->hasInactiveStatus();
    }
}
