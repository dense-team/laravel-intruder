<?php
/**
 * User: Maros Jasan
 * Date: 21.11.2019
 * Time: 13:44
 */

namespace Dense\Intruder\User;

use Illuminate\Support\Str;

trait HasApiToken
{
    /**
     * @var string
     */
    public $api_token;

    /**
     * @return $this
     */
    public function generateApiToken()
    {
        $this->api_token = Str::random(60);

        return $this;
    }

    /**
     * @return bool
     */
    public function hasApiToken()
    {
        return !is_null($this->api_token);
    }
}
