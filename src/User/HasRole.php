<?php
/**
 * User: Maros Jasan
 * Date: 21.11.2019
 * Time: 14:00
 */

namespace Dense\Intruder\User;

use Dense\Enum\Role as UserRole;

trait HasRole
{
    /**
     * @var string
     */
    public $role = UserRole::ROLE_REGULAR;

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->role === UserRole::ROLE_ADMIN;
    }

    /**
     * @return bool
     */
    public function isNotAdmin()
    {
        return !$this->isAdmin();
    }

    /**
     * @return bool
     */
    public function isRegular()
    {
        return $this->role === UserRole::ROLE_REGULAR;
    }

    /**
     * @return bool
     */
    public function isNotRegular()
    {
        return !$this->isRegular();
    }
}
