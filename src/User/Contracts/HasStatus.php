<?php
/**
 * User: Maros Jasan
 * Date: 21.11.2019
 * Time: 14:18
 */

namespace Dense\Intruder\User\Contracts;

interface HasStatus
{
    /**
     * @return bool
     */
    public function isActive();

    /**
     * @return bool
     */
    public function isInactive();
}
