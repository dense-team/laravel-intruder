<?php
/**
 * User: Maros Jasan
 * Date: 21.11.2019
 * Time: 14:18
 */

namespace Dense\Intruder\User\Contracts;

interface HasRole
{
    /**
     * @return bool
     */
    public function isAdmin();

    /**
     * @return bool
     */
    public function isRegular();
}
