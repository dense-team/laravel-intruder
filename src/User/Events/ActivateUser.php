<?php

namespace Dense\Intruder\User\Events;

use Illuminate\Queue\SerializesModels;

use Dense\Intruder\User\User;

class ActivateUser
{
    use SerializesModels;

    /**
     * @var \Dense\Intruder\User\User
     */
    public $user;

    /**
     * @param \Dense\Intruder\User\User $user
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
